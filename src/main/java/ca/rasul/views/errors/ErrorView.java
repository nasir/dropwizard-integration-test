package ca.rasul.views.errors;

import ca.rasul.representations.Saying;
import com.fasterxml.jackson.annotation.JsonValue;
import io.dropwizard.views.View;

public class ErrorView extends View{
    private final int code;
    private final String message;

    public ErrorView(int status, String message){
        super("error.mustache");
        this.code = status;
        this.message = message;
    }
    @JsonValue
    public Saying getSaying() {
        return new Saying(code, message);
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
