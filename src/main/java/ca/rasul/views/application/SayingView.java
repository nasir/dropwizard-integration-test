package ca.rasul.views.application;


import ca.rasul.representations.Saying;
import com.fasterxml.jackson.annotation.JsonValue;
import io.dropwizard.views.View;

public class SayingView extends View {
    private final Saying saying;
    public SayingView(Saying saying) {
        super("saying.mustache");
        this.saying = saying;
    }
    @JsonValue
    public Saying getSaying() {
        return this.saying;
    }
}
