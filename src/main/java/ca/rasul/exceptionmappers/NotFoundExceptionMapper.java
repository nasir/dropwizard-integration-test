package ca.rasul.exceptionmappers;
import ca.rasul.views.errors.ErrorView;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class NotFoundExceptionMapper implements ExceptionMapper<NotFoundException> {

    @Override
    public Response toResponse(NotFoundException runtime) {
        // Build default response
        //        // Check for any specific handling
        Response response = null;
        if (runtime instanceof WebApplicationException) {
            response = handleWebApplicationException(runtime);
        }

        if (response != null){
            return response;
        }

        // Use the default
        return Response
                .serverError()
                .entity(new ErrorView(404, "Not found"))
                .build();
    }

    private Response handleWebApplicationException(NotFoundException exception) {
        WebApplicationException webAppException = exception;

        // No logging
        if (webAppException.getResponse().getStatus() == 401) {
            return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .entity(new ErrorView(401, "Not found"))
                    .build();
        }
        if (webAppException.getResponse().getStatus() == 404) {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity(new ErrorView(404, "not found - 404"))
                    .build();
        }
        return null;
        // Debug logging

        // Warn logging

        // Error logging
//        LOG.error(exception, exception.getMessage());
    }
}
