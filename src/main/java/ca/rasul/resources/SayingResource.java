package ca.rasul.resources;

import ca.rasul.representations.Saying;
import ca.rasul.views.application.SayingView;
import com.codahale.metrics.annotation.Timed;
import com.google.common.base.Optional;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.concurrent.atomic.AtomicLong;

@Path("/hello-world")
@Produces({MediaType.TEXT_HTML, MediaType.APPLICATION_JSON})
public class SayingResource {
    private final String template;
    private final String defaultName;
    private final AtomicLong counter;

    public SayingResource(String template, String defaultName) {
        this.template = template;
        this.defaultName = defaultName;
        this.counter = new AtomicLong();
    }

//    @GET
//    @Timed
//    public Saying sayHello(@QueryParam("name") Optional<String> name) {
//        final String value = String.format(template, name.or(defaultName));
//        return new Saying(counter.incrementAndGet(), value);
//    }

    @GET
    @Timed
    @Path("/{name}")
    public SayingView sayHello(@PathParam("name") Optional<String> name) {
        final String value = String.format(template, name.or(defaultName));
        final Saying saying = new Saying(counter.incrementAndGet(), value);
        return new SayingView(saying);
    }
}