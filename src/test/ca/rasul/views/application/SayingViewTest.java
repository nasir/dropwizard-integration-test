package ca.rasul.views.application;

import ca.rasul.MyApplication;
import ca.rasul.MyConfiguration;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.junit.ClassRule;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class SayingViewTest {

    @ClassRule
    public static final DropwizardAppRule<MyConfiguration> RULE =
            new DropwizardAppRule<MyConfiguration>(MyApplication.class, "config.yml");
    @Test
    public void testGetSaying() throws Exception {
        assertTrue(RULE.getApplication() != null);
        assertTrue(RULE.getApplication().getName().equals("hello-world"));

        assertTrue(RULE.getConfiguration() != null);
        assertTrue(RULE.getEnvironment() != null);
        assertTrue(RULE.getLocalPort() == 9090);
    }
}