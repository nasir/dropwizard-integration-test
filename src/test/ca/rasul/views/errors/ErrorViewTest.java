package ca.rasul.views.errors;

import ca.rasul.MyApplication;
import ca.rasul.MyConfiguration;
import ca.rasul.representations.Saying;
import com.google.common.base.Optional;
import io.dropwizard.testing.junit.DropwizardAppRule;
import junit.framework.TestCase;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class ErrorViewTest extends TestCase {
    @ClassRule
    public static final DropwizardAppRule<MyConfiguration> RULE =
            new DropwizardAppRule<MyConfiguration>(MyApplication.class, "config.yml");

    MyApplication application = new MyApplication();
     {
//        File file  = new File("asd");
//        System.out.println(file.getAbsoluteFile());
//        application.run("server", "config.yml");
         try {
             application.run("server", "config.yml");
         } catch (Exception e) {
             e.printStackTrace();
         }
     }

    @Test
    public void testHelloWorldJSON() throws Exception {
        Client client = ClientBuilder.newClient();
        String name = "nana";
        WebTarget target = client.target("http://localhost:9090/hello-world").path(name);
        final Optional<String> NAME = Optional.fromNullable("Dr. IntegrationTest");

        Saying saying = target.request().accept(MediaType.APPLICATION_JSON_TYPE).get(Saying.class);
        assertTrue(saying.getContent().equals(String.format("Hello, %s!", name)));
//        assertThat(saying.getContent()).isEqualTo(RULE.getConfiguration().render(NAME));
    }
    @Test
    public void testHelloWorldHTML() throws Exception {
        Client client = ClientBuilder.newClient();
        String name = "asd";
        WebTarget target = client.target("http://localhost:9090/hello-world").path(name);
        final Optional<String> NAME = Optional.fromNullable("Dr. IntegrationTest");

        Response response = target.request().get();
        String html = target.request().accept(MediaType.TEXT_HTML).get(String.class);
        System.out.println(html);
        assertTrue(!html.isEmpty());
        assertTrue(html.contains(name));
//        assertThat(saying.getContent()).isEqualTo(RULE.getConfiguration().render(NAME));
    }
}